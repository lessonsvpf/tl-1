FROM nginx
COPY site-1 /usr/share/nginx/html/site-1 
COPY site-2 /usr/share/nginx/html/site-2 
COPY site-3 /usr/share/nginx/html/site-3 

COPY nginx-sites.conf /etc/nginx/conf.d 

EXPOSE 81 82 83 84
CMD ["nginx", "-g", "daemon off;"]
